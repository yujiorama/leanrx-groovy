import com.google.common.collect.Lists
import com.sun.javaws.exceptions.InvalidArgumentException
import org.junit.Test
import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
/*
 * http://reactive-extensions.github.io/learnrx/
 */

class Fixture {
    static def names = ["Ben", "Jafar", "Matt", "Priya", "Brian"]
    static def newReleases = [
            [
                "id": 70111470,
                "title": "Die Hard",
                "boxart": "http://cdn-0.nflximg.com/images/2891/DieHard.jpg",
                "uri": "http://api.netflix.com/catalog/titles/movies/70111470",
                "rating": 4.0,
                "bookmark": []
            ],
            [
                "id": 654356453,
                "title": "Bad Boys",
                "boxart": "http://cdn-0.nflximg.com/images/2891/BadBoys.jpg",
                "uri": "http://api.netflix.com/catalog/titles/movies/70111470",
                "rating": 5.0,
                "bookmark": [[ id:432534, time:65876586 ]]
            ],
            [
                "id": 65432445,
                "title": "The Chamber",
                "boxart": "http://cdn-0.nflximg.com/images/2891/TheChamber.jpg",
                "uri": "http://api.netflix.com/catalog/titles/movies/70111470",
                "rating": 4.0,
                "bookmark": []
            ],
            [
                "id": 675465,
                "title": "Fracture",
                "boxart": "http://cdn-0.nflximg.com/images/2891/Fracture.jpg",
                "uri": "http://api.netflix.com/catalog/titles/movies/70111470",
                "rating": 5.0,
                "bookmark": [[ id:432534, time:65876586 ]]
            ]
    ]

    static def movieLists = [
        [
            name: "New Releases",
            videos: [
                [
                    "id": 70111470,
                    "title": "Die Hard",
                    "boxart": "http://cdn-0.nflximg.com/images/2891/DieHard.jpg",
                    "uri": "http://api.netflix.com/catalog/titles/movies/70111470",
                    "rating": 4.0,
                    "bookmark": []
                ],
                [
                    "id": 654356453,
                    "title": "Bad Boys",
                    "boxart": "http://cdn-0.nflximg.com/images/2891/BadBoys.jpg",
                    "uri": "http://api.netflix.com/catalog/titles/movies/70111470",
                    "rating": 5.0,
                    "bookmark": [[ id:432534, time:65876586 ]]
                ]
            ]
        ],
        [
            name: "Dramas",
            videos: [
                [
                    "id": 65432445,
                    "title": "The Chamber",
                    "boxart": "http://cdn-0.nflximg.com/images/2891/TheChamber.jpg",
                    "uri": "http://api.netflix.com/catalog/titles/movies/70111470",
                    "rating": 4.0,
                    "bookmark": []
                ],
                [
                    "id": 675465,
                    "title": "Fracture",
                    "boxart": "http://cdn-0.nflximg.com/images/2891/Fracture.jpg",
                    "uri": "http://api.netflix.com/catalog/titles/movies/70111470",
                    "rating": 5.0,
                    "bookmark": [[ id:432534, time:65876586 ]]
                ]
            ]
        ]
    ]

    static def movieLists2 = [
            [
                name: "Instant Queue",
                videos : [
                    [
                        "id": 70111470,
                        "title": "Die Hard",
                        "boxarts": [
                            [ width: 150, height:200, url:"http://cdn-0.nflximg.com/images/2891/DieHard150.jpg" ],
                            [ width: 200, height:200, url:"http://cdn-0.nflximg.com/images/2891/DieHard200.jpg" ]
                        ],
                        "url": "http://api.netflix.com/catalog/titles/movies/70111470",
                        "rating": 4.0,
                        "bookmark": []
                    ],
                    [
                        "id": 654356453,
                        "title": "Bad Boys",
                        "boxarts": [
                            [ width: 200, height:200, url:"http://cdn-0.nflximg.com/images/2891/BadBoys200.jpg" ],
                            [ width: 150, height:200, url:"http://cdn-0.nflximg.com/images/2891/BadBoys150.jpg" ]
                        ],
                        "url": "http://api.netflix.com/catalog/titles/movies/70111470",
                        "rating": 5.0,
                        "bookmark": [[ id:432534, time:65876586 ]]
                    ]
                ]
            ],
            [
                name: "New Releases",
                videos: [
                    [
                        "id": 65432445,
                        "title": "The Chamber",
                        "boxarts": [
                            [ width: 150, height:200, url:"http://cdn-0.nflximg.com/images/2891/TheChamber150.jpg" ],
                            [ width: 200, height:200, url:"http://cdn-0.nflximg.com/images/2891/TheChamber200.jpg" ]
                        ],
                        "url": "http://api.netflix.com/catalog/titles/movies/70111470",
                        "rating": 4.0,
                        "bookmark": []
                    ],
                    [
                        "id": 675465,
                        "title": "Fracture",
                        "boxarts": [
                            [ width: 200, height:200, url:"http://cdn-0.nflximg.com/images/2891/Fracture200.jpg" ],
                            [ width: 150, height:200, url:"http://cdn-0.nflximg.com/images/2891/Fracture150.jpg" ],
                            [ width: 300, height:200, url:"http://cdn-0.nflximg.com/images/2891/Fracture300.jpg" ]
                        ],
                        "url": "http://api.netflix.com/catalog/titles/movies/70111470",
                        "rating": 5.0,
                        "bookmark": [[ id:432534, time:65876586 ]]
                    ]
                ]
            ]
        ]
    static def movieLists3 = [
            [
                    name: "New Releases",
                    videos : [
                            [
                                    "id": 70111470,
                                    "title": "Die Hard",
                                    "boxarts": [
                                            [ width: 150, height:200, url:"http://cdn-0.nflximg.com/images/2891/DieHard150.jpg" ],
                                            [ width: 200, height:200, url:"http://cdn-0.nflximg.com/images/2891/DieHard200.jpg" ]
                                    ],
                                    "url": "http://api.netflix.com/catalog/titles/movies/70111470",
                                    "rating": 4.0,
                                    "bookmark": []
                            ],
                            [
                                    "id": 654356453,
                                    "title": "Bad Boys",
                                    "boxarts": [
                                            [ width: 200, height:200, url:"http://cdn-0.nflximg.com/images/2891/BadBoys200.jpg" ],
                                            [ width: 140, height:200, url:"http://cdn-0.nflximg.com/images/2891/BadBoys140.jpg" ]
                                    ],
                                    "url": "http://api.netflix.com/catalog/titles/movies/70111470",
                                    "rating": 5.0,
                                    "bookmark": [[ id:432534, time:65876586 ]]
                            ]
                    ]
            ],
            [
                    name: "Thrillers",
                    videos: [
                            [
                                    "id": 65432445,
                                    "title": "The Chamber",
                                    "boxarts": [
                                            [ width: 130, height:200, url:"http://cdn-0.nflximg.com/images/2891/TheChamber130.jpg" ],
                                            [ width: 200, height:200, url:"http://cdn-0.nflximg.com/images/2891/TheChamber200.jpg" ]
                                    ],
                                    "url": "http://api.netflix.com/catalog/titles/movies/70111470",
                                    "rating": 4.0,
                                    "bookmark": []
                            ],
                            [
                                    "id": 675465,
                                    "title": "Fracture",
                                    "boxarts": [
                                            [ width: 200, height:200, url:"http://cdn-0.nflximg.com/images/2891/Fracture200.jpg" ],
                                            [ width: 120, height:200, url:"http://cdn-0.nflximg.com/images/2891/Fracture120.jpg" ],
                                            [ width: 300, height:200, url:"http://cdn-0.nflximg.com/images/2891/Fracture300.jpg" ]
                                    ],
                                    "url": "http://api.netflix.com/catalog/titles/movies/70111470",
                                    "rating": 5.0,
                                    "bookmark": [[ id:432534, time:65876586 ]]
                            ]
                    ]
            ]
    ]

    static def boxarts = [
            [width: 200, height: 200, url: "http://cdn-0.nflximg.com/images/2891/Fracture200.jpg" ],
            [width: 150, height: 200, url: "http://cdn-0.nflximg.com/images/2891/Fracture150.jpg" ],
            [width: 300, height: 200, url: "http://cdn-0.nflximg.com/images/2891/Fracture300.jpg" ],
            [width: 425, height: 150, url: "http://cdn-0.nflximg.com/images/2891/Fracture425.jpg" ]
    ]
}

@RunWith(Enclosed.class)
class WorkingWithArray {
    static class TraversingArray {
        def names = Lists.newArrayList(Fixture.names)
        def newReleases = Lists.newArrayList(Fixture.newReleases)

        @Test
        void "Exercise 1: Print all the names in an array"() {
            for (int counter = 0; counter < names.size(); counter++) {
                println(names[counter])
            }
        }

        @Test
        void "Exercise 2: Use forEach to print all the names in an array"() {
            names.each { name ->
                println(name)
            }
        }

        @Test
        void "Exercise 3: Project an array of videos into an array of {id,title} pairs using forEach()"() {
            def videoAndTitlePairs = []
            this.newReleases.each { release ->
                videoAndTitlePairs << ["id": release.id, "title": release.title]
            }

            assert videoAndTitlePairs.size() == 4
            assert videoAndTitlePairs.collect { it.keySet() }.flatten().sort().unique() == ["id", "title"].sort()
            assert videoAndTitlePairs.collect { it.id } == [70111470, 654356453, 65432445, 675465]
            assert videoAndTitlePairs.collect { it.title } == ["Die Hard", "Bad Boys", "The Chamber", "Fracture"]
        }

        @Test
        void "Exercise 4: Implement map()"() {
            def map = { c, f ->
                def results = []
                c.each { itemInArray ->
                    results << f(itemInArray)
                }
                return results
            }

            assert map([1, 2, 3], { x -> x + 1 }) == [2, 3, 4]
        }

        @Test
        void "Exercise 5: Use map() to project an array of videos into an array of {id,title} pairs"() {
            newReleases.metaClass.define {
                map = { f ->
                    def results = []
                    delegate.each { itemInArray ->
                        results << f(itemInArray)
                    }
                    return results
                }
            }

            assert newReleases.map({ [it.id, it.title] }) ==
                    [[70111470, "Die Hard"], [654356453, "Bad Boys"], [65432445, "The Chamber"], [675465, "Fracture"]]

        }
    }

    static class FilteringArray {
        def names = Lists.newArrayList(Fixture.names)
        def newReleases = Lists.newArrayList(Fixture.newReleases)

        @Test
        void "Exercise 6: Use forEach() to collect only those videos with a rating of 5_0"() {
            def ratingPredicate = { Math.abs(it.rating - 5.0) < 0.1 }
            assert newReleases.findAll(ratingPredicate).size() == 2
            assert newReleases.findAll(ratingPredicate).collect({ it.id }) == [654356453, 675465]
        }

        @Test
        void "Exercise 7: Implement filter()"() {
            def filter = { c, f ->
                def results = []
                c.each { itemInArray ->
                    if (f(itemInArray)) {
                        results << itemInArray
                    }
                }
                return results
            }

            assert filter([1, 2, 3], { it > 2 }) == [3]
        }
    }

    static class QueryDataByChainingMethodCalls {
        def newReleases = Lists.newArrayList(Fixture.newReleases)

        static java.util.ArrayList map(java.util.ArrayList self, Closure f) {
            def results = []
            self.each { itemInArray ->
                results << f(itemInArray)
            }
            return results
        }

        static java.util.ArrayList filter(java.util.ArrayList self, Closure f) {
            def results = []
            self.each { itemInArray ->
                if (f(itemInArray)) {
                    results << itemInArray
                }
            }
            return results
        }

        @Test
        void "Exercise 8: Chain filter and map to collect the ids of videos that have a rating of 5_0"() {
            java.util.ArrayList.mixin(QueryDataByChainingMethodCalls.class)
            assert newReleases.filter({ Math.abs(it.rating - 5.0) < 0.1 }).map({ it.id }) == [654356453, 675465]

        }
    }

    static class QueryingTrees {

        def movieLists = Lists.newArrayList(Fixture.movieLists)
        def movieLists2 = Lists.newArrayList(Fixture.movieLists2)

        static java.util.ArrayList map(java.util.ArrayList self, Closure f) {
            def results = []
            self.each { itemInArray ->
                results << f(itemInArray)
            }
            return results
        }

        static java.util.ArrayList mergeAll(java.util.ArrayList self) {
            def iter
            iter = { cc, r ->
                cc.each {
                    if (it instanceof java.util.ArrayList) {
                        r = iter(it, r)
                    } else {
                        r << it
                    }
                }
                return r
            }
            return iter(self, [])
        }

        static java.util.ArrayList filter(java.util.ArrayList self, Closure f) {
            def results = []
            self.each { itemInArray ->
                if (f(itemInArray)) {
                    results << itemInArray
                }
            }
            return results
        }

        static java.util.ArrayList flatMap(java.util.ArrayList self, Closure f) {
            self.map({ item -> f(item) }).mergeAll()
        }

        @Test
        void "Exercise 9: Flatten the movieLists array into an array of video ids"() {
            def allVideoIdsInMovieLists = []
            movieLists.each { x ->
                x.videos.each { xx ->
                    allVideoIdsInMovieLists << xx.id
                }
            } == [70111470, 654356453, 65432445, 675465]
        }

        @Test
        void "Exercise 10: Implement mergeAll()"() {
            def mergeAll = { c ->
                def iter
                iter = { cc, r ->
                    cc.each {
                        if (it instanceof java.util.ArrayList) {
                            r = iter(it, r)
                        } else {
                            r << it
                        }
                    }
                    return r
                }
                return iter(c, [])
            }

            assert mergeAll([[1, 2, 3], [4, 5, 6], [7, 8, 9]]) == [1, 2, 3, 4, 5, 6, 7, 8, 9]
        }

        @Test
        void "Exercise 11: Use map() and mergeAll() to project and flatten the movieLists into an array of video ids"() {
            java.util.ArrayList.mixin(QueryingTrees.class)
            assert movieLists.map({
                it.videos.map({ c -> c.id })
            }).mergeAll() == [70111470, 654356453, 65432445, 675465]
        }

        @Test
        void "Exercise 12: Retrieve id, title, and a 150x200 box art url for every video"() {
            java.util.ArrayList.mixin(QueryingTrees.class)
            def actual = movieLists2.map({ x ->
                x.videos.map({ xx ->
                    xx.boxarts.filter({ xxx ->
                        xxx.width == 150 && xxx.height == 200
                    }).map({ xxxx ->
                        ["id": xx.id, "title": xx.title, "boxarts": xxxx.url]
                    })
                })
            })
                    .mergeAll()
                    .sort({ it.id })

            def expected = [
                    [id: 675465, title: "Fracture", boxarts: "http://cdn-0.nflximg.com/images/2891/Fracture150.jpg"],
                    [id: 65432445, title: "The Chamber", boxarts: "http://cdn-0.nflximg.com/images/2891/TheChamber150.jpg"],
                    [id: 654356453, title: "Bad Boys", boxarts: "http://cdn-0.nflximg.com/images/2891/BadBoys150.jpg"],
                    [id: 70111470, title: "Die Hard", boxarts: "http://cdn-0.nflximg.com/images/2891/DieHard150.jpg"]
            ].sort({ it.id })

            assert actual == expected
        }

        @Test
        void "Exercise 13: Implement flatMap()"() {
            java.util.ArrayList.mixin(QueryingTrees.class)
            def flatMap = { c, f ->
                c.map({ item ->
                    f(item)
                }).mergeAll()
            }

            def spanishFrenchEnglishWords = [["cero", "rien", "zero"], ["uno", "un", "one"], ["dos", "deux", "two"]];
            // collect all the words for each number, in every language, in a single, flat list
            def actual = flatMap([0, 1, 2], { spanishFrenchEnglishWords[it] });
            def expected = ["cero", "rien", "zero", "uno", "un", "one", "dos", "deux", "two"]
            assert actual == expected
        }

        @Test
        void "Exercise 14: Use flatMap() to retrieve id, title, and 150x200 box art url for every video"() {
            java.util.ArrayList.mixin(QueryingTrees.class)
            def actual = movieLists2.flatMap({ x ->
                x.videos.flatMap({ xx ->
                    xx.boxarts.filter({ xxx ->
                        xxx.width == 150 && xxx.height == 200
                    }).map({ xxxx ->
                        ["id": xx.id, "title": xx.title, "boxarts": xxxx.url]
                    })
                })
            }).sort({ it.id })

            def expected = [
                    [id: 675465, title: "Fracture", boxarts: "http://cdn-0.nflximg.com/images/2891/Fracture150.jpg"],
                    [id: 65432445, title: "The Chamber", boxarts: "http://cdn-0.nflximg.com/images/2891/TheChamber150.jpg"],
                    [id: 654356453, title: "Bad Boys", boxarts: "http://cdn-0.nflximg.com/images/2891/BadBoys150.jpg"],
                    [id: 70111470, title: "Die Hard", boxarts: "http://cdn-0.nflximg.com/images/2891/DieHard150.jpg"]
            ].sort({ it.id })

            assert actual == expected
        }
    }

    static class ReducingArrays {
        def boxarts = Lists.newArrayList(Fixture.boxarts)
        def movieLists = Lists.newArrayList(Fixture.movieLists3)

        @Test
        void "Exercise 15: Use forEach to find the largest box art"() {
            def currentSize
            def maxSize = -1
            def largestBoxart

            boxarts.each { boxart ->
                currentSize = boxart.width * boxart.height
                if (currentSize > maxSize) {
                    largestBoxart = boxart
                    maxSize = currentSize
                }
            }

            assert largestBoxart == boxarts[3]
        }

        @Test
        void "Exercise 16: Implement reduce()"() {
            def reduce = { col, combiner, ... initialValue ->
                if (col.size() == 0) {
                    return col
                } else {
                    def counter
                    def acc
                    if (initialValue.size() == 0) {
                        counter = 1
                        acc = col[0]
                    } else if (initialValue.size() >= 1) {
                        counter = 0
                        acc = initialValue[0]
                    } else {
                        throw new InvalidArgumentException("invalid arguments")
                    }
                    while (counter < col.size()) {
                        acc = combiner(acc, col[counter++])
                    }
                    return [acc]
                }
            }
            assert [6] == reduce([1, 2, 3], { acc, value -> acc + value })
            assert [16] == reduce([1, 2, 3], { acc, value -> acc + value }, 10)
        }

        static java.util.ArrayList reduce(java.util.ArrayList self, Closure combiner, ... initialValue) {
            if (self.size() == 0) {
                return self
            } else {
                def counter
                def acc
                if (initialValue.size() == 0) {
                    counter = 1
                    acc = self[0]
                } else if (initialValue.size() >= 1) {
                    counter = 0
                    acc = initialValue[0]
                } else {
                    throw new InvalidArgumentException("invalid arguments")
                }
                while (counter < self.size()) {
                    acc = combiner(acc, self[counter++])
                }
                return [acc]
            }
        }

        static java.util.ArrayList map(java.util.ArrayList self, Closure f) {
            def results = []
            self.each { itemInArray ->
                results << f(itemInArray)
            }
            return results
        }

        @Test
        void "Exercise 17: Retrieve the largest rating"() {
            java.util.ArrayList.mixin(ReducingArrays.class)
            def ratings = [2, 3, 1, 4, 5]
            assert [5] == ratings.reduce({ acc, value ->
                if (acc < value) {
                    acc = value
                }
                return acc
            })
        }

        @Test
        void "Exercise 18: Retrieve url of the largest boxart"() {
            java.util.ArrayList.mixin(ReducingArrays.class)
            assert ["http://cdn-0.nflximg.com/images/2891/Fracture425.jpg"] ==
                    boxarts.reduce({ acc, value ->
                        if ((acc.width * acc.height) < (value.width * value.height)) {
                            acc = value
                        }
                        return acc
                    }).map({ value ->
                        value.url
                    })
        }

        @Test
        void "Exercise 19: Reducing with an initial value"() {
            java.util.ArrayList.mixin(ReducingArrays.class)
            def videos = [
                    ["id": "65432445", "title": "The Chamber"],
                    ["id": "675465", "title": "Fracture"],
                    ["id": "70111470", "title": "Die Hard"],
                    ["id": "654356453", "title": "Bad Boys"]
            ]
            def expected = [["65432445": "The Chamber", "675465": "Fracture", "70111470": "Die Hard", "654356453": "Bad Boys"]]
            def actual = videos.reduce({acc, value ->
                acc.put(value.id, value.title)
                return acc
            }, [:])
            assert actual == expected
        }

        static java.util.ArrayList mergeAll(java.util.ArrayList self) {
            def iter
            iter = { cc, r ->
                cc.each {
                    if (it instanceof java.util.ArrayList) {
                        r = iter(it, r)
                    } else {
                        r << it
                    }
                }
                return r
            }
            return iter(self, [])
        }

        static java.util.ArrayList flatMap(java.util.ArrayList self, Closure f) {
            self.map({ item -> f(item) }).mergeAll()
        }

        @Test
        void "Exercise 20: Retrieve the id, title, and smallest boxart url for every video"() {
            java.util.ArrayList.mixin(ReducingArrays.class)
            def expected = [
                    ["id": 675465, "title": "Fracture", "boxart": "http://cdn-0.nflximg.com/images/2891/Fracture120.jpg" ],
                    ["id": 65432445, "title": "The Chamber" ,"boxart": "http://cdn-0.nflximg.com/images/2891/TheChamber130.jpg" ],
                    ["id": 70111470, "title": "Die Hard" ,"boxart": "http://cdn-0.nflximg.com/images/2891/DieHard150.jpg" ],
                    ["id": 654356453, "title": "Bad Boys" ,"boxart": "http://cdn-0.nflximg.com/images/2891/BadBoys140.jpg" ],
            ]
            def actual = movieLists.flatMap({ x ->
                x.videos.flatMap({ xx ->
                    xx.boxarts.reduce({acc, xxx ->
                        if ((acc.width * acc.height) > (xxx.width * xxx.height)) {
                            acc = xxx
                        }
                        return acc
                    }).map({ xxxx ->
                        ["id": xx.id, "title": xx.title, "boxart": xxxx.url]
                    })
                })
            }).sort({it.id})

            assert actual == expected
        }
    }
}
